package model_json

import "sync"

type CachedData struct {
	data map[string]ModelJson
	rwmu sync.RWMutex
}

func NewCachedData() *CachedData {
	var cd CachedData
	cd.data = make(map[string]ModelJson)

	return &cd
}

func (cd *CachedData) Add(mj ModelJson) {
	cd.rwmu.Lock()
	defer cd.rwmu.Unlock()

	cd.data[mj.Order_uid] = mj
}

func (cd *CachedData) Get(order_uid string) (ModelJson, bool) {
	cd.rwmu.RLock()
	defer cd.rwmu.RUnlock()

	mj, exists := cd.data[order_uid]
	return mj, exists
}
