package model_json

import (
	"math/rand"
	"time"
)

func randomByteUpperLetter() byte {
	return byte(rand.Intn(26) + 65)
}

func randomByteLowerLetter() byte {
	return byte(rand.Intn(26) + 97)
}

func randomByteNumber() byte {
	return byte(rand.Intn(10) + 48)
}

func randomString(size int, randFuncs ...func() byte) string {
	bytes := make([]byte, 0, size)

	for i := 0; i < size; i++ {
		bytes = append(bytes, randFuncs[rand.Intn(len(randFuncs))]())
	}

	return string(bytes)
}

func randomName(max_size int, count_of_names int) string {
	if max_size < 3 {
		max_size = 3
	}
	if count_of_names < 1 {
		count_of_names = 1
	}
	bytes := make([]byte, 0, (max_size+1)*count_of_names+1)

	for i := 0; i < count_of_names; i++ {
		bytes = append(bytes, randomByteUpperLetter())
		for j := 0; j < rand.Intn(max_size-2)+2; j++ {
			bytes = append(bytes, randomByteLowerLetter())
		}
		if i+1 != count_of_names {
			bytes = append(bytes, byte(' '))
		}
	}

	return string(bytes)
}

func randomItem(track_number string) *Item {
	var item Item

	item.Chrt_id = rand.Intn(1000000)
	item.Track_number = track_number
	item.Price = rand.Intn(5000)
	item.Rid = randomString(20, randomByteLowerLetter)
	item.Name = randomName(10, 1)
	item.Sale = rand.Intn(50)
	item.Size = randomString(2, randomByteNumber)
	item.Total_price = item.Price * (100 - item.Sale) / 100
	item.Nm_id = rand.Intn(1000000)
	item.Brand = randomName(10, rand.Intn(5))
	item.Status = 202

	return &item
}

func RandomJSON() *ModelJson {
	var mj ModelJson
	rand.Seed(time.Now().Unix())

	mj.Order_uid = randomString(20, randomByteLowerLetter, randomByteUpperLetter, randomByteNumber)
	mj.Track_number = randomString(14, randomByteUpperLetter)
	mj.Entry = randomString(4, randomByteUpperLetter)
	mj.Delivery.Name = randomName(10, 2)
	mj.Delivery.Phone = "+" + randomString(10, randomByteNumber)
	mj.Delivery.Zip = randomString(7, randomByteNumber)
	mj.Delivery.City = randomName(20, 1)
	mj.Delivery.Address = randomName(10, rand.Intn(2)+1) + randomString(5, randomByteNumber)
	mj.Delivery.Region = randomName(20, rand.Intn(2)+1)
	mj.Delivery.Email = randomString(10, randomByteLowerLetter, randomByteNumber) + "@" + randomString(5, randomByteLowerLetter) + "." + randomString(3, randomByteLowerLetter)
	mj.Payment.Transaction = randomString(20, randomByteLowerLetter, randomByteNumber, randomByteUpperLetter)
	mj.Payment.Request_id = randomString(10, randomByteLowerLetter, randomByteNumber, randomByteUpperLetter)
	mj.Payment.Currency = randomString(3, randomByteUpperLetter)
	mj.Payment.Provider = randomString(5, randomByteLowerLetter)
	mj.Payment.Amount = rand.Intn(10000)
	mj.Payment.Payment_dt = rand.Intn(1000000)
	mj.Payment.Bank = randomString(rand.Intn(5)+2, randomByteLowerLetter)
	mj.Payment.Delivery_cost = rand.Intn(5000)
	mj.Payment.Goods_total = rand.Intn(1000)
	mj.Payment.Custom_fee = rand.Intn(1000)

	for i := 0; i < rand.Intn(5)+1; i++ {
		mj.Items = append(mj.Items, *randomItem(mj.Track_number))
	}

	mj.Locale = randomString(2, randomByteLowerLetter)
	mj.Internal_signature = randomString(rand.Intn(2), randomByteLowerLetter)
	mj.Customer_id = randomString(rand.Intn(7)+2, randomByteLowerLetter, randomByteNumber, randomByteUpperLetter)
	mj.Delivery_service = randomString(7, randomByteLowerLetter)
	mj.Shardkey = randomString(rand.Intn(2)+1, randomByteNumber)
	mj.Sm_id = rand.Intn(100)
	mj.Date_created = time.Now().Local().String()
	mj.Oof_shard = randomString(1, randomByteNumber)

	return &mj
}

func RandomTrash() string {
	return randomString(rand.Intn(999)+1, randomByteLowerLetter, randomByteUpperLetter, randomByteNumber)
}
