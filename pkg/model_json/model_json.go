package model_json

import (
	"encoding/json"
	"errors"
)

type Delivery struct {
	Name    string `json:"name"`
	Phone   string `json:"phone"`
	Zip     string `json:"zip"`
	City    string `json:"city"`
	Address string `json:"address"`
	Region  string `json:"region"`
	Email   string `json:"email"`
}

type Payment struct {
	Transaction   string `json:"transaction"`
	Request_id    string `json:"request_id"`
	Currency      string `json:"currency"`
	Provider      string `json:"provider"`
	Bank          string `json:"bank"`
	Amount        int    `json:"amount"`
	Payment_dt    int    `json:"payment_dt"`
	Delivery_cost int    `json:"delivery_cost"`
	Goods_total   int    `json:"goods_total"`
	Custom_fee    int    `json:"custom_fee"`
	// TODO: optimize structure memory
}

type Item struct {
	Track_number string `json:"track_number"`
	Rid          string `json:"rid"`
	Name         string `json:"name"`
	Size         string `json:"size"` // wtf
	Brand        string `json:"brand"`
	Chrt_id      int    `json:"chrt_id"`
	Price        int    `json:"price"`
	Total_price  int    `json:"total_price"`
	Nm_id        int    `json:"nm_id"`
	Status       int    `json:"status"`
	Sale         int    `json:"sale"`
	// TODO: optimize structure memory
}

type ModelJson struct {
	Delivery           Delivery `json:"delivery"`
	Payment            Payment  `json:"payment"`
	Items              []Item   `json:"items"`
	Order_uid          string   `json:"order_uid"`
	Track_number       string   `json:"track_number"`
	Entry              string   `json:"entry"`
	Locale             string   `json:"locale"`
	Internal_signature string   `json:"internal_signature"`
	Customer_id        string   `json:"customer_id"`
	Delivery_service   string   `json:"delivery_service"`
	Shardkey           string   `json:"shardkey"`
	Date_created       string   `json:"date_created"`
	Oof_shard          string   `json:"oof_shard"`
	Sm_id              int      `json:"sm_id"`
	// TODO: optimize structure memory
}

func (mj *ModelJson) Marshal() ([]byte, error) {
	bytes, err := json.Marshal(mj)
	return bytes, err
}

func UnMarshal(data []byte, mj *ModelJson) error {
	if !json.Valid(data) {
		return errors.New("Received invalid json.")
	}

	err := json.Unmarshal(data, mj)
	return err
}

func (left *ModelJson) Equal(right *ModelJson) (bool, error) {
	if left == nil || right == nil {
		return false, errors.New("One of the comparable is nil. Not nil parameters required")
	}

	if left == right {
		return false, nil
	}

	return left.Customer_id == right.Customer_id && left.Date_created == left.Date_created &&
		left.Delivery_service == right.Delivery_service && left.Entry == right.Entry &&
		left.Internal_signature == right.Internal_signature && left.Locale == right.Locale &&
		left.Oof_shard == right.Oof_shard && left.Order_uid == right.Order_uid &&
		left.Shardkey == right.Shardkey && equalItems(left.Items, right.Items) &&
		equalPayment(left.Payment, right.Payment) && equalDelivery(left.Delivery, right.Delivery), nil
}

func equalItems(left, right []Item) bool {
	if len(left) != len(right) {
		return false
	}
	for i, l := range left {
		r := right[i]
		if l.Brand != r.Brand && l.Chrt_id != r.Chrt_id &&
			l.Name != r.Name && l.Nm_id != r.Nm_id &&
			l.Price != r.Price && l.Rid != r.Rid &&
			l.Sale != r.Sale && l.Size != r.Size &&
			l.Status != r.Status && l.Total_price != r.Total_price &&
			l.Track_number != r.Track_number {
			return false
		}
	}
	return true
}

func equalPayment(left, right Payment) bool {
	return left.Amount == right.Amount && left.Bank == right.Bank &&
		left.Currency == right.Currency && left.Custom_fee == right.Custom_fee &&
		left.Delivery_cost == right.Delivery_cost && left.Goods_total == right.Goods_total &&
		left.Payment_dt == right.Payment_dt && left.Provider == right.Provider &&
		left.Request_id == right.Request_id && left.Transaction == right.Transaction
}

func equalDelivery(left, right Delivery) bool {
	return left.Address == right.Address && left.City == right.City &&
		left.Email == right.Email && left.Name == right.Name &&
		left.Phone == right.Phone && left.Region == right.Region &&
		left.Zip == right.Zip
}
