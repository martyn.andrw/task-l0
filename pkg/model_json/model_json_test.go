package model_json

import (
	"testing"
)

func checkErr(t *testing.T, err error) {
	if err != nil {
		t.Errorf("Got an error: %s\n", err)
	}
}

func TestMarshalUnMarshal(t *testing.T) {
	mj := RandomJSON()
	mj_bytes, err := mj.Marshal()
	checkErr(t, err)

	var mj_new ModelJson
	err = UnMarshal(mj_bytes, &mj_new)
	checkErr(t, err)

	equal, err := mj.Equal(&mj_new)
	checkErr(t, err)
	if !equal {
		t.Errorf("Output %q not equal to expected %q", mj_new, mj)
	}
}

func TestRandomData(t *testing.T) {
	rj := RandomJSON()

	if rj == nil {
		t.Errorf("Output %q is nil, expected random data", rj)
	}

	if rj.Order_uid == "" {
		t.Errorf("Output %q contains zero values, which shouldn't be zero values", rj)
	}
}

func TestCachedData(t *testing.T) {
	cd := NewCachedData()
	if cd == nil {
		t.Errorf("Output %q is nil, expected not nil", cd)
	}

	rj := RandomJSON()
	if rj == nil {
		t.Errorf("Output %q is nil, expected random data", rj)
	}

	cd.Add(*rj)
	mj, ok := cd.Get(rj.Order_uid)
	if !ok {
		t.Errorf("Output %q is zero values, which shouldn't be zero values ", mj)
	}
	equal, err := rj.Equal(&mj)
	checkErr(t, err)
	if !equal {
		t.Errorf("Got %q, but expected %q", mj, rj)
	}
}
