package postgresql

import (
	"context"
	"database/sql"
	"fmt"
	"strconv"

	"gitlab.com/martyn.andrw/task-l0/pkg/model_json"
)

func writeDelivery(db *sql.DB, delivery model_json.Delivery) (int, error) {
	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if checkErrorRollback(err, tx) {
		return 0, err
	}
	defer tx.Commit()

	query :=
		`INSERT INTO delivery ("name_us", "phone", "zip", "city", "address", "region", "email") 
		VALUES ($1, $2, $3, $4, $5, $6, $7)
		RETURNING id`
	stmt, err := db.Prepare(query)
	if checkErrorRollback(err, tx) {
		return 0, err
	}
	defer stmt.Close()

	var id int
	err = stmt.QueryRow(
		delivery.Name, delivery.Phone, delivery.Zip, delivery.City,
		delivery.Address, delivery.Region, delivery.Email).Scan(&id)
	if checkErrorRollback(err, tx) {
		return 0, err
	}

	return id, nil
}

func writePayment(db *sql.DB, payment model_json.Payment) error {
	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if checkErrorRollback(err, tx) {
		return err
	}
	defer tx.Commit()

	insertPayment := fmt.Sprintf(
		`INSERT INTO payment VALUES ('%s', '%s', '%s', '%s', '%d', '%d', '%s', '%d', '%d', '%d');`,
		payment.Transaction, payment.Request_id, payment.Currency, payment.Provider, payment.Amount,
		payment.Payment_dt, payment.Bank, payment.Delivery_cost, payment.Goods_total, payment.Custom_fee,
	)

	_, err = tx.ExecContext(ctx, insertPayment)
	if checkErrorRollback(err, tx) {
		return err
	}
	return nil
}

func writeItems(db *sql.DB, items []model_json.Item) (string, error) {
	result := "{"
	for _, item := range items {
		ctx := context.Background()
		tx, err := db.BeginTx(ctx, nil)
		if checkErrorRollback(err, tx) {
			return result + "}", err
		}
		defer tx.Commit()

		insertPayment := fmt.Sprintf(
			`INSERT INTO items VALUES ('%d', '%d', '%s', '%s', '%d', '%s', '%d', '%d', '%s', '%d');`,
			item.Chrt_id, item.Price, item.Rid, item.Name, item.Sale, item.Size,
			item.Total_price, item.Nm_id, item.Brand, item.Status,
		)

		_, err = tx.ExecContext(ctx, insertPayment)
		if checkErrorRollback(err, tx) {
			return result + "}", err
		}
		if result != "{" {
			result += ", "
		}
		result += strconv.Itoa(item.Chrt_id)
	}
	return result + "}", nil
}

func writeOrder(db *sql.DB, mj model_json.ModelJson, deliveryId int, strItems string) error {
	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if checkErrorRollback(err, tx) {
		return err
	}
	defer tx.Commit()

	insertPayment := fmt.Sprintf(
		`INSERT INTO user_order VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%s', '%d', '%s', '%s');`,
		mj.Order_uid, mj.Track_number, mj.Entry, mj.Locale, mj.Internal_signature,
		mj.Customer_id, mj.Delivery_service, mj.Shardkey, mj.Sm_id, mj.Date_created,
		mj.Oof_shard, deliveryId, mj.Payment.Transaction, strItems,
	)

	_, err = tx.ExecContext(ctx, insertPayment)
	if checkErrorRollback(err, tx) {
		return err
	}
	return nil
}

func Write(db *sql.DB, mj model_json.ModelJson) error {
	deliveryId, err := writeDelivery(db, mj.Delivery)
	if err != nil {
		return err
	}

	writePayment(db, mj.Payment)
	str, err := writeItems(db, mj.Items)
	writeOrder(db, mj, deliveryId, str)
	return nil
}
