package postgresql

import (
	"database/sql"
	"fmt"
	"log"
	"time"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "l0_user"
	password = "l0"
	dbname   = "l0"
)

func NewClient(maxAattemps int) (db *sql.DB, err error) {
	for maxAattemps > 0 {
		psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
		db, err = sql.Open("postgres", psqlconn)
		if err == nil && db != nil {
			break
		}
		time.Sleep(200 * time.Millisecond)
	}
	return
}

func checkErrorRollback(err error, tx *sql.Tx) bool {
	if err != nil {
		log.Println(err)
		tx.Rollback()
		return true
	}
	return false
}
