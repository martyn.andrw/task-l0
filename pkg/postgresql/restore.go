package postgresql

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/lib/pq"
	"gitlab.com/martyn.andrw/task-l0/pkg/model_json"
)

func getItems(db *sql.DB, items pq.Int64Array) ([]model_json.Item, error) {
	mjItems := make([]model_json.Item, 0, len(items))
	for _, item := range items {
		ctx := context.Background()
		tx, err := db.BeginTx(ctx, nil)
		if err != nil {
			return nil, err
		}
		defer tx.Commit()

		selectItem := fmt.Sprintf("SELECT * FROM items WHERE chrt_id = %d", item)
		resI, err := tx.Query(selectItem)
		if checkErrorRollback(err, tx) {
			return nil, err
		}
		defer resI.Close()

		if resI.Next() {
			var mjItem model_json.Item

			err = resI.Scan(
				&mjItem.Chrt_id, &mjItem.Price, &mjItem.Rid, &mjItem.Name, &mjItem.Sale, &mjItem.Size,
				&mjItem.Total_price, &mjItem.Nm_id, &mjItem.Brand, &mjItem.Status,
			)
			if checkErrorRollback(err, tx) {
				return nil, err
			}
			mjItems = append(mjItems, mjItem)
		}

	}
	return mjItems, nil
}

func getPayment(db *sql.DB, transaction_ string) (model_json.Payment, error) {
	var payment model_json.Payment
	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		tx.Rollback()
		return payment, err
	}
	defer tx.Commit()

	selectTransaction := fmt.Sprintf("SELECT * FROM payment WHERE transaction_ = '%s'", transaction_)
	resT, err := tx.Query(selectTransaction)
	if checkErrorRollback(err, tx) {
		return payment, err
	}
	defer resT.Close()
	if resT.Next() {
		err = resT.Scan(
			&payment.Transaction, &payment.Request_id, &payment.Currency,
			&payment.Provider, &payment.Amount, &payment.Payment_dt,
			&payment.Bank, &payment.Delivery_cost, &payment.Goods_total,
			&payment.Custom_fee,
		)
	}
	return payment, err
}

func getDelivery(db *sql.DB, delivery int) (model_json.Delivery, error) {
	var mjDelivery model_json.Delivery
	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if checkErrorRollback(err, tx) {
		return mjDelivery, err
	}
	defer tx.Commit()

	var uselessId int
	selectDelivery := fmt.Sprintf("SELECT * FROM delivery WHERE id = %d", delivery)
	resD, err := tx.Query(selectDelivery)
	if checkErrorRollback(err, tx) {
		return mjDelivery, err
	}
	defer resD.Close()
	if resD.Next() {
		err = resD.Scan(
			&uselessId, &mjDelivery.Name, &mjDelivery.Phone, &mjDelivery.Zip,
			&mjDelivery.City, &mjDelivery.Address, &mjDelivery.Region,
			&mjDelivery.Email,
		)
	}
	return mjDelivery, err
}

func Restore() (pCD *model_json.CachedData, err error) {
	db, err := NewClient(5)
	if err != nil {
		return model_json.NewCachedData(), err
	}
	defer db.Close()

	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if checkErrorRollback(err, tx) {
		return nil, err
	}
	defer tx.Commit()

	pCD = model_json.NewCachedData()
	selectUsrOrd := "SELECT * FROM user_order"
	res, err := tx.Query(selectUsrOrd)
	if checkErrorRollback(err, tx) {
		return pCD, err
	}
	defer res.Close()

	for res.Next() {
		var (
			transaction_ string
			delivery     int
			items        pq.Int64Array
		)
		defer res.Close()

		var mj model_json.ModelJson
		err = res.Scan(
			&mj.Order_uid, &mj.Track_number, &mj.Entry,
			&mj.Locale, &mj.Internal_signature, &mj.Customer_id,
			&mj.Delivery_service, &mj.Shardkey, &mj.Sm_id,
			&mj.Date_created, &mj.Oof_shard, &delivery,
			&transaction_, &items,
		)
		if checkErrorRollback(err, tx) {
			return pCD, err
		}

		mj.Items, err = getItems(db, items)
		if checkErrorRollback(err, tx) {
			return pCD, err
		}

		mj.Payment, err = getPayment(db, transaction_)
		if checkErrorRollback(err, tx) {
			return pCD, err
		}

		mj.Delivery, err = getDelivery(db, delivery)
		if checkErrorRollback(err, tx) {
			return pCD, err
		}

		pCD.Add(mj)
	}

	if checkErrorRollback(err, tx) {
		return pCD, err
	}
	return
}
