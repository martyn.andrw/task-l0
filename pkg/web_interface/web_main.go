package web_interface

import (
	"log"
	"net/http"
	"text/template"

	"gitlab.com/martyn.andrw/task-l0/pkg/model_json"
)

var (
	pData *model_json.CachedData
)

func getOrder(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		t, err := template.ParseFiles("pkg/web_interface/templates/getOrder.html")
		if err != nil {
			log.Println("Web: ", err)
			return
		}
		t.Execute(w, nil)
	} else {
		t, err := template.ParseFiles("pkg/web_interface/templates/getOrder.html")
		if err != nil {
			log.Println("Web: ", err)
			return
		}
		id := r.FormValue("id")
		mj, exists := (*pData).Get(id)

		if exists {
			t.Execute(w, mj)
		} else {
			t.Execute(w, nil)
		}
	}
}

func ListenAndServe(pointerData *model_json.CachedData) {
	pData = pointerData
	http.HandleFunc("/getorder", getOrder)

	http.ListenAndServe(":8080", nil)
}
