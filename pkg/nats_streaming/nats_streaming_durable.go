package nats_streaming

import (
	"fmt"
	"log"
	"strings"

	"github.com/nats-io/stan.go"
)

const (
	clusterID   = "l0-cluster"
	clientID    = "l0-sub"
	URL         = stan.DefaultNatsURL
	channel     = "l0"
	durable     = "l0-durable"
	unsubscribe = false
)

func ListenAndWait(dataChan chan []byte, cleanupChan chan struct{}) {
	sc, err := stan.Connect(clusterID, clientID,
		stan.SetConnectionLostHandler(func(_ stan.Conn, reason error) {
			log.Fatalf("NATS-streaming: Connection lost, reason: %v", reason)
		}))
	if err != nil {
		log.Fatalf("NATS-streaming: Can't connect: %v.\nMake sure a NATS Streaming Server is running at: %s", err, URL)
	}
	log.Printf("NATS-streaming: Connected to [%s] clusterID: [%s] clientID: [%s]\n", URL, clusterID, clientID)

	// Unsubscribe will cause the server to completely remove the durable subscription.
	_, err = sc.Subscribe(channel, func(m *stan.Msg) {
		msg := string(m.Data)
		start := strings.Index(msg, "\"order_uid\"")
		end := start + 34

		if start != -1 {
			fmt.Printf("NATS-streaming: Received a message: %s\n\n", msg[start:end])
		} else {
			fmt.Printf("NATS-streaming: Received a message: %s\n\n", msg)
		}
		dataChan <- m.Data
	}, stan.DurableName(durable))
	if err != nil {
		log.Println(err)
	}

cleanup:
	for range cleanupChan {
		log.Println("NATS-streaming: Received an interrupt, closing connection...")
		err = sc.Close()
		if err != nil {
			log.Println("NATS-streaming got an error while closing. err: \n\t", err)
		} else {
			log.Print("NATS-streaming: Connection has been closed...\n\n")
			close(dataChan)
			break cleanup
		}
	}
}
