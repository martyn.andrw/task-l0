package main

import (
	"log"
	"os"

	"github.com/nats-io/stan.go"
	"gitlab.com/martyn.andrw/task-l0/pkg/model_json"
)

const (
	clusterID   = "l0-cluster"
	clientID    = "l0-pub"
	URL         = stan.DefaultNatsURL
	channel     = "l0"
	durable     = "l0-durable"
	unsubscribe = false
)

func main() {
	sc, err := stan.Connect(clusterID, clientID)
	if err != nil {
		log.Fatalln(err)
	}

	var bytes []byte
	send := os.Args[1]
	switch send {
	case "r":
		rd := *model_json.RandomJSON()
		bytes, err = rd.Marshal()
		if err != nil {
			log.Println(err)
		}

	case "t":
		bytes = []byte(model_json.RandomTrash())
	}

	err = sc.Publish(channel, bytes) // does not return until an ack has been received from NATS Streaming
	if err != nil {
		log.Println(err)
	}
	log.Printf("Published in [%s] : '%s'\n", durable, bytes)
}
