NS_PATH=/home/andrew/go/pkg/mod/github.com/nats-io

.PHONY: rns
rns: run-nats-streaming

.PHONY: run-nats-streaming
run-nats-streaming:
	go run $(NS_PATH)/nats-streaming-server@v0.24.6/nats-streaming-server.go --stan_config nats-streaming.conf

rtm: run-temp_main

run-temp_main:
	go run temp_main.go

rrs: run-random_sender

run-random_sender:
	go run sender/main.go r internal/model_json

rts: run-trash_sender

run-trash_sender:
	go run sender/main.go t internal/model_json

rm: run-main

run-main:
	go run main.go