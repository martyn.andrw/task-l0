package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"

	"gitlab.com/martyn.andrw/task-l0/pkg/model_json"
	"gitlab.com/martyn.andrw/task-l0/pkg/nats_streaming"
	"gitlab.com/martyn.andrw/task-l0/pkg/postgresql"
	"gitlab.com/martyn.andrw/task-l0/pkg/web_interface"
)

var (
	PCachedData *model_json.CachedData
)

func cleanup(signalChan chan os.Signal, cleanupChan chan struct{}) {
	for range signalChan {
		fmt.Printf("\nReceived an interrupt, attempting to close all connections...\n\n")
		cleanupChan <- struct{}{}
		cleanupChan <- struct{}{}
		time.Sleep(200 * time.Millisecond)
	}
}

func listenAndWrite(dataChan chan model_json.ModelJson, cleanupChan chan struct{}) {
	db, err := postgresql.NewClient(5)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("PostgreSQL: ready")

writeLoop:
	for {
		select {
		case <-cleanupChan:
			break writeLoop
		case mj := <-dataChan:
			postgresql.Write(db, mj)
		default:
			time.Sleep(200 * time.Millisecond)
		}
	}

	log.Println("PostgreSQL: Received an interrupt, closing connection...")
	close(dataChan)
	err = db.Close()
	if err != nil {
		log.Println("PostgreSQL got an error while closing. err: \n\t", err)
	} else {
		log.Print("PostgreSQL: Connection has been closed...\n\n")
	}
}

func main() {
	dataChan := make(chan []byte)
	mjChan := make(chan model_json.ModelJson)
	cleanupChan := make(chan struct{})
	signalChan := make(chan os.Signal, 1)

	signal.Notify(signalChan, os.Interrupt)
	PCachedData, err := postgresql.Restore()
	if err != nil {
		PCachedData = model_json.NewCachedData()
		log.Println(err)
	}

	go nats_streaming.ListenAndWait(dataChan, cleanupChan)
	go web_interface.ListenAndServe(PCachedData)
	go listenAndWrite(mjChan, cleanupChan)
	go cleanup(signalChan, cleanupChan)

	var mj model_json.ModelJson
	// listenLoop:
	for data := range dataChan {
		if err := model_json.UnMarshal(data, &mj); err != nil {
			log.Println(err)
		} else {
			PCachedData.Add(mj)
			mjChan <- mj
		}
	}
	<-mjChan
}
